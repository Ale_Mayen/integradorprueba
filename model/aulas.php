<?php


include_once("database.php");

class Aulas{
    
    private $pdo;    
    public $N_aula;
    public $latitud;
    public $longitud;
	 public $detalle;

	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT N_aula, latitud, longitud, detalle FROM aulas");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function getByID($N_aula)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT N_aula, latitud, longitud, detalle FROM aulas WHERE N_aula = ?");
			          

			$stm->execute(array($N_aula));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function del($data)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM aulas WHERE N_aula = ?");			          

			$stm->execute(array($data->N_aula));
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE aulas SET 
						latitud = ?, 
						longitud = ?,
						detalle = ?
				    WHERE N_aula = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->latitud, 
                        $data->longitud,
						$data->detalle,
                        $data->N_aula
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add(Aulas $data)
	{
		try{
		$sql = "INSERT INTO aulas(latitud,longitud,detalle) 
		        VALUES (?, ?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->latitud, 
                    $data->longitud,
					$data->detalle
					
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}