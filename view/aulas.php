<?php

?>




<table class="table table-striped">
    <thead>
        <tr>
            <th >ID aulas</th>
            <th>latitud</th>
            <th>longitud</th>
			 <th>detalle</th>
            <th ><img src="Assets/img/editar.png" border="1" width="30" height="30"/></th>
            <th ><img src="Assets/img/eliminar.png" border="1" width="30" height="30"/></th>
        </tr>
    </thead>
    <tbody>
	<?php foreach($this->model->getAll() as $r): ?>
        <tr>
	    <td><?php echo $r->N_aula; ?></td>
            <td><?php echo $r->latitud; ?></td>
            <td><?php echo $r->longitud; ?></td>
			 <td><?php echo $r->detalle; ?></td>
            <td>
                <a href="?controller=Aulas&accion=Crud&N_aula=<?php echo $r->N_aula; ?>">Editar</a>
            </td>
            <td>
               <a href="?controller=Aulas&accion=Del&N_aula=<?php echo $r->N_aula; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 

<div>
    <a href="?controller=Aulas&accion=Crud">Nueva Aula</a>
</div>
